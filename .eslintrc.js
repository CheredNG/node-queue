module.exports = {
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": ["eslint:recommended", "standard"],
    "plugins": [
        "jsdoc"
    ],
    "rules": {
        "indent": [
            "error",
            4,
            {"SwitchCase":1}
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "error",
            "single",
            "avoid-escape"
        ],
        "semi": [
            "error",
            "always"
        ],
        "camelcase": 0,
        "no-console": 0,
        "no-labels": 0,
        "no-extra-boolean-cast":0,
        "no-mixed-operators": 0,
        "require-jsdoc": [
            "error",
            {
                "require": {
                    "FunctionDeclaration": true,
                    "MethodDefinition": true,
                    "ClassDeclaration": true,
                    "ArrowFunctionExpression": false
                }
            }
        ],
        "valid-jsdoc": [
            "error",
            {
                "preferType": {
                    "object": "Object"
                },
                "requireReturn": true,
                "requireParamDescription": true,
                "requireReturnDescription": true
            }
        ]
    },
    "globals":{
        "_":true,
        "vms":true,
        "HttpError":true,
        "async":true,
        "util": true,
        "it": true,
        "describe": true
    },
};