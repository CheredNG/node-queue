'use strict';

const Bull = require('bull');

let bull;
let name;

/**
 * Собираем воркера и начинаем поглощать очередь.
 * @param {string} queue имя очереди для поглощения
 * @param {string} newName имя вокера
 * @return {undefined}
 */
function main (queue, newName) {
    bull = new Bull(queue);
    name = newName;
    consume();
}

/**
 * Поглощаем очередь.
 * @return {undefined}
 */
function consume () {
    bull.process(function (job, jobDone) {
        let msg = `Сообщение "${job.data.msg}" ` +
            `получено worker'ом ${name}` +
            `, ${job.id}`;
        console.log(msg);
        jobDone();
    });
}

module.exports = main;
