'use strict';

const Bull = require('bull');
const Redis = require('redis');

let channel;

let bull;
let subscriber;

/**
 * Создаем подписчика редиса и начинаем получать сообщения.
 * @param {string} newChannel канал для сабскрайба
 * @param {string} newQueue очередь, в которую передаем сообщения
 * @return {undefined}
 */
function main (newChannel, newQueue) {
    channel = newChannel;
    subscriber = Redis.createClient();
    bull = new Bull(newQueue);
    subscribe();
}

/**
 * Получаем сообщения и передаем их в очередь редиса.
 * @return {undefined}
 */
function subscribe () {
    subscriber.subscribe(channel);
    subscriber.on('message', (channel, msg) => {
        sendToQueue(msg);
    });
}

/**
 * Передаем сообщение в очередь редиса.
 * @param {string} msg сообщение в очередь
 * @return {undefined}
 */
function sendToQueue (msg) {
    bull.add({ msg });
}

module.exports = main;
