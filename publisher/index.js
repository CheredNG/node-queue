'use strict';

const Redis = require('redis');

let channel;

let publisher;

/**
 * Создаем клиента редиса и начинаем выдавать сообщения.
 * @param {string} newChannel имя канала для паблиша
 * @return {undefined}
 */
function main (newChannel) {
    channel = newChannel;
    publisher = Redis.createClient();
    publish();
}

/**
 * Публикуем сообщения в редис.
 * @return {undefined}
 */
function publish () {
    for (let i = 1; i <= 100000; i++) {
        publisher.publish(channel, `Message ${i}`);
    }
}

module.exports = main;
