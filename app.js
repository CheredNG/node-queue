'use strict';

const Cluster = require('cluster');
const numWorkers = require('os').cpus().length;

const channel = 'pubsub';
const queue = 'flood';

if (Cluster.isMaster) {
    for (let i = 0; i < numWorkers; i++) {
        Cluster.fork();
    }
    require('./subscriber')(channel, queue);
    require('./publisher')(channel);
} else {
    require('./worker')(queue, Cluster.worker.id);
}
